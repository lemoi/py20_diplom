import requests
from pprint import pprint
import json
import time
import math

PATH_ENV = 'env.json'

class User:
  def __init__(self, token, id=None):
    self.token = token
    self.last_query = None
    self.log_error = []

    if id != None:
      if type(id) == int or str(id).isdigit():
        self.id = int(id)
      else:
        self.domain = id

    self.info = self.get_user_info()
    self.id = self.info['id']
    self.domain = self.info['domain']

  def appeal_api(self, method, params):
    union_params = {
      'access_token': self.token,
      'v': '5.92',
    }
    union_params.update(params)
    url = 'https://api.vk.com/method/' + method

    # API VK позволяет делать не более 3 запросов в секунду. Добавим необходимую задержку между двумя запросам
    # ! Необходимо дополнительно добавить проверку ответа на превышение числа запросов и повторную отправку после паузы
    sleep_time = 0 if self.time_last_query == None else self.time_last_query + 0.34 - time.time()
    if sleep_time > 0:
      time.sleep(sleep_time)

    # Сделаем три попытки получения данных в случае получения ошибки превышения числа запросов к API за секунду
    # На самом деле предыдущего ограничения достаточно, чтобы ошибка с кодом 6 не приходила
    for i in range(3):
      response = requests.get(url, params=union_params)

      self.time_last_query = time.time()

      result = response.json()
      if 'error' in result:
        self.log_error.append(result)

      if 'error' in result and result['error']['error_code'] == 6:
        time.sleep(0.34)  # немного подождем перед следующим запросом
      else:
        break

    self.last_query = result
    if 'error'in result:
      print('Ошибка обращения к API VK')
      pprint(result['error'])
      raise

    return result.get('response')

  def __str__(self):
    return f'https://vk.com/id{self.id}'

  def __and__(self, other):
    params = {
      'source_uid': self.id,
      'target_uid': other.id
    }

    result = self.appeal_api('friends.getMutual', params)
    users = [User(self.token, user_id) for user_id in result]

    return users;

  def __getattr__(self, name):
    if name == 'info':
      self.info = self.get_user_info()
      return self.info

  def get_user_info(self, name_case=None):
    params = {
      'fields': 'domain'
    }

    if self.id != None:
      params['user_ids'] = self.id
    elif self.domain != None:
      params['user_ids'] = self.domain

    if name_case != None:
      params['name_case'] = name_case

    result = self.appeal_api('users.get', params)
    return result[0]

  def get_status(self):
    params = {
      'user_id': self.id
    }

    result = self.appeal_api('status.get', params)
    return result.get('text')

  def get_unique_groups(self, min_friends=0):

    try:
      params = {
        'user_id': self.id,
        # 'fields': 'domain'
      }

      friends = self.appeal_api('friends.get', params).get('items')

    except Exception as e:
      return []

    try:
      params = {
        'user_id': self.id,
        # 'filter': 'groups',
        'extended': 1,
        'fields': 'members_count'
      }

      groups = self.appeal_api('groups.get', params).get('items')

    except Exception as e:
      return []


    step = 2
    max_friends_method = 500
    friend_parts = math.ceil(len(friends) / max_friends_method)
    count = step + len(groups) * friend_parts
    show_progress(f"friends: {len(friends)}. groups: {len(groups)}", step, count)

    result = []
    for group in groups:
      members = []

      for part in range(friend_parts):
        friends_str = ','.join([str(item) for item in friends[part * max_friends_method : (part + 1) * max_friends_method]])
        params_to_member = {
          'group_id': group['id'],
          'user_ids': friends_str,
        }

        group_members = self.appeal_api('groups.isMember', params_to_member)

        step += 1
        show_progress(f"process group {group['id']}", step, count)

        members.extend([item['user_id'] for item in group_members if item['member']])

      if len(members) <= min_friends:
        result.append({
          'name': group['name'],
          'gid': group['id'],
          'members_count': group['members_count'],
          'friends_count': len(members),
          'members_friend': [f'https://vk.com/id{id}' for id in members],
          })

    return result


def get_env():
  with open(PATH_ENV, 'r', encoding='utf-8') as file:
    env = json.load(file)

  return env

def update_env(env):
  with open(PATH_ENV, 'w', encoding='utf-8') as file:
    json.dump(env, file, indent=2)

def show_progress(text, step, count):
    # percent = 100 if count == 0 else step / count * 100
    # message = f'\r{text}. {percent:.0f}%'
    message = f'\r{text}. step {step} is {count}'
    print(message + (' ' * (80 - len(message))), end='')
    if step == count:
      print()


if __name__ == '__main__':
  PATH_ENV = 'env.example.json'
  env = get_env()
  # Действительный токен необходимо поместить в параметр token, либо установить тут
  # env['token'] = ''

  victim = env['user_1']
  # victim = env['user_2']

  user = User(env['token'], victim)
  # pprint(user.info)

  result = user.get_unique_groups(5)
  # pprint(result)
  # pprint(user.log_error)

  with open('groups.json', 'wt') as file:
    json.dump(result, file, indent=4)
    # json.dump(result, file, indent=4, ensure_ascii=False)
